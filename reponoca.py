#!/usr/bin/env python

import operator

stack = []
optrs = {'+': operator.add, '-': operator.sub,
         '*': operator.mul, '/': operator.div}


def chkexp(tokens):
    for token in tokens:
        try:
            # lets check for the symbols first
            if token in optrs:
                if len(stack) < 2:
                    raise ValueError('Need two values for calc to work')
                x = stack.pop()
                y = stack.pop()
                op = optrs[token]
                stack.append(op(y, x))
            else:
                stack.append(float(token))
        except ZeroDivisionError:
            print ("Error: Its divide by zero!!")
            return stack
        except ValueError, e:
            print "Error: " + str(e)
            return stack
    return stack

if __name__ == '__main__':
    # lets run the calc in a loop
    while True:
        exp = raw_input('> ')
        if exp in ['quit', 'q', 'exit']:
            exit()
        elif exp in ['clear', 'empty']:
            stack = []
            print 'Current Stack=> ' + str(stack)
            continue
        elif len(exp) == 0:
            continue
        stack = chkexp(exp.split())
        print 'Current Stack=> ' + str(stack)
